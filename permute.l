# Picolisp
(de permute (Source-list)
  (cond
    # กรณีที่ Source-list ไม่ใช่ list หรือมีสมาชิกน้อยกว่า 1 ตัว 
    ((or (not (lst? Source-list)) (not Source-list)) '(()))
    # กรณีที่มีสมาชิกใน list น้อยกว่า 2 ตัว
    ((= (length Source-list) 1) (list (list (car Source-list))))
    # กรณีที่มีสมาชิกใน list แค่ 2 ตัว
    ((= (length Source-list) 2) 
      (let Permutations (list (copy Source-list))
        (when (n== (car Source-list) (cadr Source-list))
          (queue 'Permutations (list (cadr Source-list) (car Source-list))))
        Permutations))
    # กรณีที่มีสมาชิกใน list มากกว่า 2 ตัว
    (T
      # set ของสมาชิกที่ไม่่ซ้ำใน list
      (let (Permutations () Set-of-source-member ())
        (for Source-index (length Source-list)
          (let Source-member (get Source-list Source-index)
            (when (not (member Source-member Set-of-source-member))
              (push1 'Set-of-source-member Source-member)
              # หา permutation ของ list ใหม่ที่ตัดเอาสมาชิกที่ตำแหน่งที่ Source-index ออก
              # เอาสมาชิกใน list เดิมตำแหน่งที่ Source-index ใส่กลับเข้าไปใหม่
              (setq Permutation-of-sublist (mapcar
                                             '((Sublist)
                                               (cons Source-member Sublist))
                                             (permute (remove Source-index Source-list))))
              (for Permutation-index (length Permutation-of-sublist)
                (queue 'Permutations (get Permutation-of-sublist Permutation-index))))))
        Permutations))))

(let A (permute (1 2 2 3)) (for V A (println V)))
(let B (permute (2 3 4)) (for V B (println V)))
(bye)
