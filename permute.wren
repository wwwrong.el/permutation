var permute
permute = Fn.new {|source_list|
    // กรณีที่ source_list ไม่ใช่ array
    if (!(source_list is List)) return [[]]
    // กรณีที่มีสมาชิกใน list น้อยกว่า 2 ตัว
    if (source_list.count < 2) return [source_list[0..-1]]
    var all_permute = []
    // กรณีที่มีสมาชิกใน list แค่ 2 ตัว
    if (source_list.count == 2) {
        all_permute.add(source_list[0..-1])    
        if (source_list[0] != source_list[1]) {
          all_permute.add([source_list[1], source_list[0]])
        }
        return all_permute 
    } 
    // กรณีที่มีสมาชิกใน list มากกว่า 2 ตัว
    // set ของสมาชิกที่ไม่ซ้าใน list
    var set = {}
    for (i in 0...source_list.count) {
      // เลือกหา permutation โดยตัดกรณีที่สมาชิกที่ซ้ำออก
      if (!set[source_list[i]]) {
          set[source_list[i]] = true 
          // หา permutation ของ list ใหม่ที่ตัดเอาสมาชิกที่ตำแหน่งที่ i ออก
          var plist = permute.call(source_list[0...i] + source_list[(i+1)..-1])
          // เอาสมาชิกใน list เดิมตำแหน่งที่ i ใส่กลับเข้าไปใหม่
          for(j in plist) all_permute.add([source_list[i]] + j) 
      }
    } 
    return all_permute 
}

var a = permute.call([1, 2, 2, 4])
a.each {|x| System.print(x)}
var b = permute.call({1: 1, 2: 2, 3: 2, 4: 4})
b.each {|x| System.print(x)}
