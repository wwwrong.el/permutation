<?php
function permute($source_list){
    $size = count($source_list);
    // กรณีที่ไม่ใช่ sequential array
    if(array_keys($source_list) != range(0, count($source_list)-1))
        return [[]];
    // กรณีที่มีสมาชิกใน list น้อยกว่า 2 ตัว
    if($size < 2)
        return [$source_list];
    $all_permute = [];
    // กรณีที่มีสมาชิกใน list แค่ 2 ตัว
    if($size == 2){
        array_push($all_permute,$source_list);
        if($source_list[0]!==$source_list[1]){ 
            array_push($all_permute, [$source_list[1], $source_list[0]]); 
        } 
        return $all_permute;
    }
    // กรณีที่มีสมาชิกใน list มากกว่า 2 ตัว
    $set = [];
    for($i=0; $i < $size; $i++){
        // เลือกหา permutation โดยตัดกรณีที่สมาชิกที่ซ้ำออก
        if(!in_array($source_list[$i], $set, true)){
            array_push($set, $source_list[$i]); 
            // สร้าง list ใหม่ที่ตัดเอาสมาชิกที่ตำแหน่งที่ i ออก
            $tmp = $source_list;
            array_splice($tmp, $i, 1); 
            // หา permutation ของ list ใหม่
            $plist = permute($tmp);
            // เอาสมาชิกใน list เดิมตำแหน่งที่ i ใส่กลับเข้าไปใหม่
            foreach($plist as $j){
                array_unshift($j, $source_list[$i]);
                array_push($all_permute, $j);
            }
        }
    }
    return $all_permute; 
}

$a = permute([1, 2, 2, 4]);
foreach($a as $x){echo "[".implode(", ", $x)."]\n";}
$b = permute([1=>1,2=>2,3=>2,4=>4]);
foreach($b as $x){echo "[".implode(", ", $x)."]\n";}
