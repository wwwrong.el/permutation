concat = table.concat
insert = table.insert
remove = table.remove
unpack = unpack or table.unpack

permute = (source_list) ->
  -- กรณีที่ source_list ไม่ใช่ table
  if (type source_list) ~= "table" then return {{}}
  -- กรณีที่มีสมาชิกใน list น้อยกว่า 2 ตัว
  if #source_list < 2
    return source_list[1] and {{source_list[1]}} or {{}}
  all_permute = {}
  -- กรณีที่มีสมาชิกใน list แค่ 2 ตัว
  if #source_list == 2
    all_permute[1] = {source_list[1], source_list[2]}
    if source_list[1] ~= source_list[2]
      all_permute[2] = {source_list[2], source_list[1]}
    return all_permute
  -- กรณีที่มีสมาชิกใน list มากกว่า 2 ตัว
  -- set ของสมาชิกที่ไม่ซ้าใน list
  set = {}
  for i = 1, #source_list
    -- เลือกหา permutation โดยตัดกรณีที่สมาชิกที่ซ้ำออก
    if not set[source_list[i]]
      set[source_list[i]] = true
      -- สร้าง list ใหม่ที่ตัดเอาสมาชิกที่ตำแหน่งที่ i ออก
      tmp = {unpack source_list}
      remove tmp, i
      -- หา permutation ของ list ใหม่
      plist = permute tmp
      -- เอาสมาชิกใน list เดิมตำแหน่งที่ i ใส่กลับเข้าไปใหม่
      for j = 1, #plist
        insert plist[j], 1, source_list[i]
        all_permute[#all_permute + 1] = plist[j]
  all_permute

a = permute {1, 2, 2, 4}
for i=1,#a do print "{"..concat(a[i], ", ").."}"
b = permute {[0]:1, [1]:2, [2]:2, [3]:4, a:3}
for i=1,#b do print "{"..concat(b[i], ", ").."}"
