function permute(source_list)
  # กรณีที่ source_list ไม่ใช่ array
  if !isa(source_list, Array) return [[]] end
  # กรณีที่มีสมาชิกใน list น้อยกว่า 2 ตัว
  if length(source_list) < 2 return [source_list[1:end]] end
  all_permute = []
  # กรณีที่มีสมาชิกใน list แค่ 2 ตัว
  if length(source_list) == 2
    push!(all_permute, source_list[1:end])
    if source_list[1] != source_list[2]
      push!(all_permute, [source_list[2], source_list[1]])
    end
    return all_permute
  end
  # กรณีที่มีสมาชิกใน list มากกว่า 2 ตัว
  # set ของสมาชิกที่ไม่ซ้าใน list
  slist = Set()
  for i in 1:length(source_list)
    # เลือกหา permutation โดยตัดกรณีที่สมาชิกที่ซ้ำออก
    if !in(source_list[i], slist)
      push!(slist, source_list[i])
      # สร้าง list ใหม่ที่ตัดเอาสมาชิกที่ตำแหน่งที่ i ออก
      tmp = source_list[1:end]
      splice!(tmp,i)
      # หา permutation ของ list ใหม่
      plist = permute(tmp)
      # เอาสมาชิกใน list เดิมตำแหน่งที่ i ใส่กลับเข้าไปใหม่
      for j in plist
        pushfirst!(j, source_list[i])
        push!(all_permute, j)
      end
    end
  end
  all_permute
end

a = permute([1, 2, 2, 4])
for x in a println(x) end
b = permute(Dict(1=>1, 2=>2, 3=>2, 4=>4))
for x in b  println(x) end
