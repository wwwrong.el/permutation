def permute(source_list)
    # กรณีที่ source_list ไม่ใช่ array
    if source_list.class != Array then return [[]] end
    # กรณีที่มีสมาชิกใน list น้อยกว่า 2 ตัว
    if source_list.size < 2 then return [source_list.clone] end
    all_permute = []
    # กรณีที่มีสมาชิกใน list แค่ 2 ตัว
    if source_list.size == 2 
        all_permute.push(source_list[0..-1])    
        if source_list[0] != source_list[1] 
            all_permute.push([source_list[1], source_list[0]])
        end 
        return all_permute 
    end 
    # กรณีที่มีสมาชิกใน list มากกว่า 2 ตัว
    # set ของสมาชิกที่ไม่ซ้าใน list
    set = {}
    source_list.each_index do |i|
      # เลือกหา permutation โดยตัดกรณีที่สมาชิกที่ซ้ำออก
      if not set[source_list[i]] 
          set[source_list[i]] = true 
          # หา permutation ของ list ใหม่ที่ตัดเอาสมาชิกที่ตำแหน่งที่ i ออก
          plist = permute(i==0 ? source_list[1..-1] : (source_list[0..(i-1)] + source_list[(i+1)..-1]))
          # เอาสมาชิกใน list เดิมตำแหน่งที่ i ใส่กลับเข้าไปใหม่
          plist.each {|j| all_permute.push([source_list[i]] + j)} 
      end
    end 
    all_permute 
end

a=permute([1, 2, 2, 4])
a.each {|x| print x, "\n"}
b=permute({1=>1, 2=>2, 3=>2, 4=>4})
b.each {|x| print x, "\n"}
