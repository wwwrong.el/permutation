function permute(source_list){
    // กรณีที่ source_list ไม่ใช่ array
    if(!Array.isArray(source_list)) return [[]];
    var size = source_list.length
    // กรณีที่มีสมาชิกใน list น้อยกว่า 2 ตัว
    if(size < 2) return [source_list.slice()];
        //return [[...source_list]];
    var all_permute = [];
    // กรณีที่มีสมาชิกใน list แค่ 2 ตัว
    if(size == 2){
        all_permute.push(source_list.slice());
        //all_permute.push([...source_list]);
        if(source_list[0] !== source_list[1]){ 
            all_permute.push([source_list[1], source_list[0]]);
        } 
        return all_permute;
     }
    // กรณีที่มีสมาชิกใน list มากกว่า 2 ตัว
    var set = {};
    for(var i=0; i < size; i++){
        // เลือกหา permutation โดยตัดกรณีที่สมาชิกที่ซ้ำออก
        if (!(set[source_list[i]])){
            set[source_list[i]] = true; 
            // หา permutation ของ list ใหม่ที่ตัดเอาสมาชิกที่ตำแหน่งที่ i ออก
            var plist = permute(source_list.slice(0,i).concat(source_list.slice(i+1)));
            // เอาสมาชิกใน list เดิมตำแหน่งที่ i ใส่กลับเข้าไปใหม่
            for (j of plist){
               j.unshift(source_list[i]);
               all_permute.push(j);
            }
        }
    }
    return all_permute; 
}

var a = permute([1, 2, 2, 4]);
for(x of a){console.log(x);}
var b = permute({1:1, 2:2, 3:2, 4:4});
for(x of b){console.log(x);}
