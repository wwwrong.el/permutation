(local concat table.concat)
(local insert table.insert)
(local remove table.remove)

(fn permute [source-list]
  (var permutations [])
  (if
    ;กรณีที่ source-list ไม่ใช่ table
    (not= (type source-list) :table) [[]]
    ;กรณีที่มีสมาชิกใน list น้อยกว่า 2 ตัว
    (< (length source-list) 2) (or (and (. source-list 1) [[(. source-list 1)]]) [[]])
    ;กรณีที่มีสมาชิกใน list แค่ 2 ตัว
    (= (length source-list) 2)
      (do
        (tset permutations 1 [(. source-list 1) (. source-list 2)])
        (when (not= (. source-list 1) (. source-list 2))
          (tset permutations 2 [(. source-list 2) (. source-list 1)]))
        permutations)
    ;กรณีที่มีสมาชิกใน list มากกว่า 2 ตัว
    (do
      ;set ของสมาชิกที่ไม่ซ้าใน list
      (var set-of-source-member [])
      (for [i 1 (length source-list)]
        ;เลือกหา permutation โดยตัดกรณีที่สมาชิกที่ซ้ำออก
        (when (not (. set-of-source-member (. source-list i)))
          (tset set-of-source-member (. source-list i) true)
          ;สร้าง list ใหม่ที่ตัดเอาสมาชิกที่ตำแหน่งที่ i ออก
          (var [& sublist] source-list)
          (remove sublist i)
          ;หา permutation ของ list ใหม่
          (var permutation-of-sublist (permute sublist))
          ;เอาสมาชิกใน list เดิมตำแหน่งที่ i ใส่กลับเข้าไปใหม่
          (for [j 1 (length permutation-of-sublist)]
            (insert (. permutation-of-sublist j) 1 (. source-list i))
            (tset permutations (+ (length permutations) 1) (. permutation-of-sublist j)))))
      permutations)))

(var a (permute [1 2 2 4]))
(for [i 1 (length a)]
  (print (.. "{" (concat (. a i) ", ") "}")))
(var b (permute {0 1 1 2 2 2 3 4 :a 3}))
(for [i 1 (length b)]
  (print (.. "{" (concat (. b i) ", ") "}")))
