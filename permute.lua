local concat = table.concat
local insert = table.insert
local remove = table.remove
local unpack = unpack or table.unpack

function permute(source_list)
  -- กรณีที่ source_list ไม่ใช่ table
  if type(source_list) ~= "table" then return {{}} end
  -- กรณีที่มีสมาชิกใน list น้อยกว่า 2 ตัว
  if #source_list < 2 then
    return source_list[1] and {{source_list[1]}} or {{}}
  end
  local permutations = {}
  -- กรณีที่มีสมาชิกใน list แค่ 2 ตัว
  if #source_list == 2 then
    permutations[1] = {source_list[1], source_list[2]}
    if source_list[1] ~= source_list[2] then
      permutations[2] = {source_list[2], source_list[1]}
    end
    return permutations
  end
  -- กรณีที่มีสมาชิกใน list มากกว่า 2 ตัว
  -- set ของสมาชิกที่ไม่ซ้าใน list
  local set_of_source = {}
  for i = 1, #source_list do
    -- เลือกหา permutation โดยตัดกรณีที่สมาชิกที่ซ้ำออก
    if not set_of_source[source_list[i]] then
      set_of_source[source_list[i]] = true
      -- สร้าง list ใหม่ที่ตัดเอาสมาชิกที่ตำแหน่งที่ i ออก
      local sublist = {unpack(source_list)}
      remove(sublist, i)
      -- หา permutation ของ list ใหม่
      local permutation_sublist = permute(sublist)
      -- เอาสมาชิกใน list เดิมตำแหน่งที่ i ใส่กลับเข้าไปใหม่
      for j = 1, #permutation_sublist do
        insert(permutation_sublist[j], 1, source_list[i])
        permutations[#permutations + 1] = permutation_sublist[j]
      end
    end
  end
  return permutations
end

local a = permute {1, 2, 2, 4}
for i=1,#a do print("{"..concat(a[i], ", ").."}") end
local b = permute {[0]=1, [1]=2, [2]=2, [3]=4, a=3}
for i=1,#b do print("{"..concat(b[i], ", ").."}") end
