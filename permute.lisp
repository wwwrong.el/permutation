(defun remove-list (lst indx)
  (if (= indx 0)
      (cdr lst)
      (cons (car lst) (remove-list (cdr lst) (- indx 1)))))

(defun permute (source-list)
  (cond
    ; กรณีที่ source-list ไม่ใช่ list หรือมีสมาชิกน้อยกว่า 1 ตัว 
    ((or (not (listp source-list)) (not source-list)) '(()))
    ; กรณีที่มีสมาชิกใน list น้อยกว่า 2 ตัว
    ((= (length source-list) 1) (list (list (car source-list))))
    ; กรณีที่มีสมาชิกใน list แค่ 2 ตัว
    ((= (length source-list) 2) 
      (let ((permutations (list (copy-list source-list))))
        (when (not (equal (car source-list) (cadr source-list)))
              (nconc permutations (list (list (cadr source-list) (car source-list))))
        permutations)))
    ; กรณีที่มีสมาชิกใน list มากกว่า 2 ตัว
    (T
      ; set ของสมาชิกที่ไม่่ซ้ำใน list
      (let ((permutations         '())
            (set-of-source-member '())
            (source-index         -1))
        (dolist (source-member source-list)
          (when (not (member source-member set-of-source-member))
            (if (not set-of-source-member)
                (setq set-of-source-member (list source-member))
                (nconc set-of-source-member (list source-member)))
            (incf source-index)
            ; หา permutation ของ list ใหม่ที่ตัดเอาสมาชิกที่ตำแหน่งที่ source-index ออก
            ; เอาสมาชิกใน list เดิมตำแหน่งที่ source-index ใส่กลับเข้าไปใหม่
            (setq permutation-of-sublist (mapcar
                                           (lambda (sublist)
                                             (cons source-member sublist))
                                           (permute (remove-list source-list source-index))))
            (if (not permutations)
                (setq permutations permutation-of-sublist)
                (nconc permutations permutation-of-sublist))))
        permutations))))

(let ((a (permute '(1 2 2 3)))) (dolist (v a) (print v)))
(let ((b (permute '(2 3 4)))) (dolist (v b) (print v)))
(quit)
