;; test in Tinyscheme
(define remove-list
  (lambda (lst indx)
    (if (= indx 0)
        (cdr lst)
        (cons (car lst) (remove-list (cdr lst) (- indx 1))))))

(define get-permute
  (lambda (i set-of-lst-memb lst lsts)
    (cond
      ((< i (length lst))
        (let ((v (list-ref lst i)))
          ; set ของสมาชิกที่ไม่ซ้ำใน list
          (when (not (member v set-of-lst-memb))
            (set! set-of-lst-memb (append set-of-lst-memb (list v)))
            (define sub-lst (remove-list lst i))
            (define permut-of-sub-lst (permute sub-lst))
            (set! lsts (append lsts (map (lambda (p) (cons v p)) permut-of-sub-lst)))))
        (get-permute (+ i 1) set-of-lst-memb lst lsts))
      (else lsts))))

(define (permute source-list)
  (cond
    ; กรณีที่ source-list ไม่ใช่ list หรือมีสมาชิกน้อยกว่า 1 ตัว 
    ((or (not (list? source-list)) (= (length source-list) 0)) '(()))
    ; กรณีที่มีสมาชิกใน list น้อยกว่า 2 ตัว
    ((= (length source-list) 1) (list (list (car source-list))))
    ; กรณีที่มีสมาชิกใน list แค่ 2 ตัว
    ((= (length source-list) 2)
      ;(define permutations (cons (car source-list) (cdr source-list)))
      (define permutations (list (list-tail source-list 0)))
      ;(when (not (equal? (list-ref source-list 0) (list-ref source-list 1)))
      (when (not (equal? (car source-list) (cadr source-list)))
        (set! permutations (append permutations (list (list (cadr source-list) (car source-list))))))
      permutations)
    ; กรณีที่มีสมาชิกใน list มากกว่า 2 ตัว
    (else
      (get-permute 0 '() source-list '()))))

(let ((a (permute '(1 2 2 3))))
  (for-each (lambda(v) (display v) (newline)) a))

(let ((b (permute '(2 3 4))))
  (for-each (lambda(v) (display v) (newline)) b))
