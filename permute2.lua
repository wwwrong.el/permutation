local concat = table.concat
local insert = table.insert
local remove = table.remove
local unpack = unpack or table.unpack

local function permute_one(list) return list[1] and {{list[1]}} or {{}} end

local function permute_two(list)
  local permutations = {{list[1], list[2]}}
  if list[1] ~= list[2] then
    permutations[2] = {list[2], list[1]}
  end
  return permutations
end

local function insert_member(list, value, return_list)
  for list_index = 1, #list do
    insert(list[list_index], 1, value)
    return_list[#return_list + 1] = list[list_index]
  end
end

local function permute_multi(source_list)
  local permutations = {}
  local set_of_source_member = {}
  for source_index = 1, #source_list do
    if not set_of_source_member[source_list[source_index]] then
      set_of_source_member[source_list[source_index]] = true
      local permutation = {unpack(source_list)}
      local source_member_at_index = remove(permutation, source_index)
      local permutation_sublist = permute(permutation)
      insert_member(permutation_sublist, source_member_at_index, permutations)
    end
  end
  return permutations
end

function permute(source_list)
  if type(source_list) ~= "table" then return {{}} end
  if #source_list < 2 then return permute_one(source_list) end
  if #source_list == 2 then return permute_two(source_list) end
  return permute_multi(source_list)
end

local a = permute {1, 2, 2, 4}
for i=1,#a do print("{"..concat(a[i], ", ").."}") end
local b = permute {[0]=1, [1]=2, [2]=2, [3]=4, a=3}
for i=1,#b do print("{"..concat(b[i], ", ").."}") end
