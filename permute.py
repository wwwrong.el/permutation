def permute(source_list):
    # กรณีที่ไม่ใช่ list
    if type(source_list) != list: return [[]]
    # กรณีที่มีสมาชิกใน list น้อยกว่า 2 ตัว
    if len(source_list) < 2: return [source_list[::]]
    all_permute = []   
    # กรณีที่มีสมาชิกใน list แค่ 2 ตัว
    if len(source_list) == 2:
        all_permute.append([source_list[0], source_list[1]])
        if source_list[0] != source_list[1]: 
            all_permute.append([source_list[1], source_list[0]]) 
        return all_permute 
    # กรณีที่มีสมาชิกใน list มากกว่า 2 ตัว
    temp_list = []
    for i in range(len(source_list)):
        # เลือกหา permutation โดยตัดกรณีที่สมาชิกที่ซ้ำออก
        if not source_list[i] in temp_list:
            temp_list.append(source_list[i]) 
            # หา permutation ของ list ใหม่ที่ตัดเอาสมาชิกที่ตำแหน่งที่ i ออก
            plist = permute(source_list[:i:] + source_list[i+1::])
            # เอาสมาชิกใน list เดิมตำแหน่งที่ i ใส่กลับเข้าไปใหม่
            for j in plist:
               j.insert(0,source_list[i])
               all_permute.append(j)
    return all_permute 

a = permute([1, 2, 2, 4])
for x in a: print(x)
b = permute({1:1, 2:2, 3:2, 4:4})
for x in b: print(x)
